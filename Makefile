

SRC = MurrayHeymann
COMPILE = pdflatex
BIBLIO = biber

pdf:
	$(COMPILE) $(SRC)
	#bibtex $(SRC)
	$(BIBLIO) $(SRC)
	$(COMPILE) $(SRC)
	$(COMPILE) $(SRC)

quick:
	pdflatex $(SRC)


all: pdf

clean:
	rm -fvr *.aux
	rm -fvr *.bbl
	rm -fvr *.toc
	rm -fvr *.idx
	rm -fvr *.lof
	rm -fvr *.blg
	rm -fvr *.bcf
	rm -fvr *.log
	rm -fvr *~
	rm -fvr *.out
	rm -fvr *.run.xml
	rm -fvr *.ilg
	rm -fvr *.ind
	rm -fvr *.lot
	rm -fvr *.loa
